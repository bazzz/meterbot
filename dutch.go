package meterbot

var daynames = map[string]string{
	"Sun": "zondag",
	"Mon": "maandag",
	"Tue": "dinsdag",
	"Wed": "woensdag",
	"Thu": "donderdag",
	"Fri": "vrijdag",
	"Sat": "zaterdag",
}

func DayName(input string) string {
	if name, ok := daynames[input]; ok {
		return name
	}
	return ""
}

func TodayName() string {
	return "vandaag"
}

func YesterdayName() string {
	return "gisteren"
}
