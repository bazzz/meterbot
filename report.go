package meterbot

type Report struct {
	Labels  []string `json:"labels"`
	Values  []int    `json:"values"`
	Colors  []string `json:"colors"`
	Borders []string `json:"borders"`
}
