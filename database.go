package meterbot

import (
	"database/sql"
	"strconv"
	"time"

	_ "github.com/lib/pq" // postgresql
)

type database struct {
	db *sql.DB
}

func newDatabase(username, password, host, name string) (*database, error) {
	dbType := "postgres"
	connStr := dbType + "://" + username + ":" + password + "@" + host + "/" + name
	if dbType == "postgres" {
		sslmode := "disable"
		connStr += "?sslmode=" + sslmode + "&fallback_application_name=" + appName
	}
	db, err := sql.Open(dbType, connStr)
	if err != nil {
		return nil, err
	}
	db.SetConnMaxLifetime(1 * time.Hour)
	d := database{
		db: db,
	}
	return &d, nil
}

func (d *database) storeElectricityReading(timestamp time.Time, usedTariff1, usedTariff2, currentusage int) error {
	query := `
		INSERT INTO "electricity" (timestamp, usedtariff1, usedtariff2, currentusage) 
		VALUES ($1, $2, $3, $4)
	`
	if _, err := d.db.Exec(query, timestamp, usedTariff1, usedTariff2, currentusage); err != nil {
		return err
	}
	return nil
}

func (d *database) storeElectricityReadingWithDelivery(timestamp time.Time, usedTariff1, usedTariff2, deliveredTariff1, deliveredTariff2, currentusage, currentdelivery int) error {
	query := `
		INSERT INTO "electricity" (timestamp, usedtariff1, usedtariff2, deliveredtariff1, deliveredtariff2, currentusage, currentdelivery) 
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	if _, err := d.db.Exec(query, timestamp, usedTariff1, usedTariff2, deliveredTariff1, deliveredTariff2, currentusage, currentdelivery); err != nil {
		return err
	}
	return nil
}

func (d *database) storeGasReading(timestamp time.Time, used int) error {
	query := `
		INSERT INTO "gas" (timestamp, used, currentusage) 
		VALUES ($1, $2, $3 - COALESCE((SELECT MAX(used) FROM "gas"),0))
	`
	if _, err := d.db.Exec(query, timestamp, used, used); err != nil {
		return err
	}
	return nil
}

type Usage struct {
	Timestamp time.Time
	Value     int
}

func (d *database) GetRecentDailyElectricity() ([]Usage, error) {
	return d.getRecentDaily("electricity")
}

func (d *database) GetRecentDailyGas() ([]Usage, error) {
	return d.getRecentDaily("gas")
}

func (d *database) getRecentDaily(t string) ([]Usage, error) {
	formula := `MAX("used")-MIN("used")`
	if t == "electricity" {
		formula = `(MAX("usedtariff1")-MIN("usedtariff1"))+(MAX("usedtariff2")-MIN("usedtariff2"))`
	}
	query := `
		SELECT "timestamp"::date, ` + formula + ` FROM ` + t + `
		GROUP BY "timestamp"::date
		ORDER BY "timestamp"::date DESC
		LIMIT 8
	`
	return d.getUsage(query)
}

func (d *database) GetDayGas(day int) ([]Usage, error) {
	query := `
		SELECT date_trunc('hour', "timestamp"), "currentusage" from gas
		WHERE "timestamp"::date = (current_date - INTEGER '` + strconv.Itoa(day) + `')::date
		GROUP BY date_trunc('hour', "timestamp"), "currentusage"
		ORDER BY date_trunc('hour', "timestamp") DESC
	`
	return d.getUsage(query)
}

func (d *database) GetDayElectricity(day int) ([]Usage, error) {
	query := `
		SELECT date_trunc('hour', "timestamp"), (max("usedtariff1")-min("usedtariff1"))+(max("usedtariff2")-min("usedtariff2")) from electricity
		WHERE "timestamp"::date = (current_date - INTEGER '` + strconv.Itoa(day) + `')::date
		GROUP BY date_trunc('hour', "timestamp")
		ORDER BY date_trunc('hour', "timestamp") DESC
	`
	return d.getUsage(query)
}

func (d *database) getUsage(query string) ([]Usage, error) {
	rows, err := d.db.Query(query)
	if err != nil {
		return nil, err
	}
	usages := make([]Usage, 0)
	for rows.Next() {
		usage := Usage{}
		err := rows.Scan(&usage.Timestamp, &usage.Value)
		if err != nil {
			return usages, err
		}
		usages = append(usages, usage)
	}
	// reverse
	for i, j := 0, len(usages)-1; i < j; i, j = i+1, j-1 {
		usages[i], usages[j] = usages[j], usages[i]
	}
	return usages, nil
}
