package meterbot

import (
	"embed"
	"html/template"
	"log"
	"os"
	"path/filepath"
)

var (
	//go:embed assets/*
	assetsFS embed.FS

	//go:embed templates/*
	templatesFS embed.FS

	templates *template.Template
	appName   string
)

func init() {
	tpls, err := template.ParseFS(templatesFS, "templates/*.gohtml")
	if err != nil {
		log.Fatal("init could not parse templates:", err)
	}
	templates = tpls

	execFile, err := os.Executable()
	if err != nil {
		log.Fatal("init could not app name:", err)
	}
	name := filepath.Base(execFile)
	ext := filepath.Ext(appName)
	if ext != "" {
		name = name[:len(name)-len(ext)]
	}
	appName = name
}
