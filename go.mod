module gitlab.com/bazzz/meterbot

go 1.18

require (
	github.com/lib/pq v1.10.5
	gitlab.com/bazzz/cfg v0.0.0-20211008083346-0087e658624c
	gitlab.com/bazzz/dsmr v0.0.0-20220412090754-ea366ab74f6d
	gitlab.com/bazzz/log v0.0.0-20220303073850-7bf406fe6b81
)

require (
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07 // indirect
	golang.org/x/sys v0.0.0-20220412071739-889880a91fd5 // indirect
)
