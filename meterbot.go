package meterbot

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/bazzz/dsmr"
)

// Meterbot is the meterbot client.
type MeterBot struct {
	client        *dsmr.Client
	latestReading dsmr.Telegram
	settings      Settings
	database      *database
}

// New instantiates a configured Meterbot client.
func New(settings Settings) (*MeterBot, error) {
	meterbot := MeterBot{
		settings: settings,
	}
	if settings.SerialPortName != "" {
		client, err := dsmr.New(settings.SerialPortName, settings.DSMRProtocolVersion)
		if err != nil {
			return nil, fmt.Errorf("could not instantiate dsmr client: %w", err)
		}
		client.OnRead = meterbot.readHandler
		meterbot.client = client
	}

	db, err := newDatabase(settings.DatabaseUsername, settings.DatabasePassword, settings.DatabaseHost, settings.DatabaseName)
	if err != nil {
		return nil, fmt.Errorf("could not connect to database: %w", err)
	}
	meterbot.database = db

	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.FS(assetsFS))))
	http.HandleFunc("/", meterbot.handleHome)
	http.HandleFunc("/gas/", meterbot.handleGas)
	http.HandleFunc("/electricity/", meterbot.handleElectricity)
	return &meterbot, nil
}

// Start starts listening.
func (m *MeterBot) Start() (err error) {
	log.Println("Start")
	if m.client != nil {
		go func() {
			if e := m.client.Listen(); e != nil {
				log.Println("ERROR:", err)
				return
			}
		}()
	}
	host := m.settings.Host
	port := m.settings.Port
	return http.ListenAndServe(host+":"+strconv.Itoa(port), nil)
}

func (m *MeterBot) readHandler(telegram dsmr.Telegram) {
	if m.latestReading.Raw == "" {
		m.latestReading = telegram
	}
	if telegram.TimeStamp.Hour() != m.latestReading.TimeStamp.Hour() {
		var err error
		if m.settings.Delivery {
			err = m.database.storeElectricityReadingWithDelivery(telegram.TimeStamp, int(telegram.Electricity.UsedTariff1.Value), int(telegram.Electricity.UsedTariff2.Value), int(telegram.Electricity.DeliveredTariff1.Value), int(telegram.Electricity.DeliveredTariff2.Value), int(telegram.Electricity.CurrentUsage.Value), int(telegram.Electricity.CurrentDelivery.Value))
		} else {
			err = m.database.storeElectricityReading(telegram.TimeStamp, int(telegram.Electricity.UsedTariff1.Value), int(telegram.Electricity.UsedTariff2.Value), int(telegram.Electricity.CurrentUsage.Value))
		}
		if err != nil {
			log.Println("ERROR: could not store electricity readings:", err)
		}
		timestamp := telegram.Gas.TimeStamp
		if m.settings.GasMeterDelay > 0 {
			timestamp = timestamp.Add(time.Duration(-1*m.settings.GasMeterDelay) * time.Hour)
		}
		if err := m.database.storeGasReading(timestamp, int(telegram.Gas.Used.Value*1000.0)); err != nil {
			log.Println("ERROR: could not store gas reading:", err)
		}
	}
	m.latestReading = telegram
}

func (m *MeterBot) GetRecentDailyGas() (report Report) {
	report.Colors = []string{"hsla(42, 100%, 35%, 0.2)"}
	report.Borders = []string{"hsla(42, 100%, 35%, 1)"}
	usages, err := m.database.GetRecentDailyGas()
	if err != nil {
		log.Println("ERROR: could not get recent daily gas:", err)
	}
	now := time.Now()
	for _, usage := range usages {
		label := DayName(usage.Timestamp.Format("Mon"))
		if now.Day() == usage.Timestamp.Day() {
			label = TodayName()
		}
		if now.Add(-1*(24*time.Hour)).Day() == usage.Timestamp.Day() {
			label = YesterdayName()
		}
		report.Labels = append(report.Labels, label)
		report.Values = append(report.Values, usage.Value)
	}
	return
}

func (m *MeterBot) GetRecentDailyElectricity() (report Report) {
	report.Colors = []string{"hsla(165, 100%, 35%, 0.2)"}
	report.Borders = []string{"hsla(165, 100%, 35%, 1)"}
	usages, err := m.database.GetRecentDailyElectricity()
	if err != nil {
		log.Println("ERROR: could not get recent daily electricity:", err)
	}
	now := time.Now()
	for _, usage := range usages {
		label := DayName(usage.Timestamp.Format("Mon"))
		if now.Day() == usage.Timestamp.Day() {
			label = TodayName()
		}
		if now.Add(-1*(24*time.Hour)).Day() == usage.Timestamp.Day() {
			label = YesterdayName()
		}
		report.Labels = append(report.Labels, label)
		report.Values = append(report.Values, usage.Value)
	}
	return
}

// GetDayGas returns a report for the specified day, where today is 0, yesterday 1, the day before yesterday 2, etc.
func (m *MeterBot) GetDayGas(day int) (report Report) {
	report.Colors = []string{"hsla(42, 100%, 35%, 0.2)"}
	report.Borders = []string{"hsla(42, 100%, 35%, 1)"}
	usages, err := m.database.GetDayGas(day)
	if err != nil {
		log.Println("ERROR: could not get day gas:", err)
	}
	for _, usage := range usages {
		label := usage.Timestamp.Format("15:04")
		report.Labels = append(report.Labels, label)
		report.Values = append(report.Values, usage.Value)
	}
	return
}

// GetDayElectricity returns a report for the specified day, where today is 0, yesterday 1, the day before yesterday 2, etc.
func (m *MeterBot) GetDayElectricity(day int) (report Report) {
	report.Colors = []string{"hsla(165, 100%, 35%, 0.2)"}
	report.Borders = []string{"hsla(165, 100%, 35%, 1)"}
	usages, err := m.database.GetDayElectricity(day)
	if err != nil {
		log.Println("ERROR: could not get day electricity:", err)
	}
	for _, usage := range usages {
		label := usage.Timestamp.Format("15:04")
		report.Labels = append(report.Labels, label)
		report.Values = append(report.Values, usage.Value)
	}
	return
}
