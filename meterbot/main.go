package main

import (
	"log"

	"gitlab.com/bazzz/cfg"
	bazzzlog "gitlab.com/bazzz/log"
	"gitlab.com/bazzz/meterbot"
)

func main() {
	bazzzlog.Connect()
	log.SetOutput(bazzzlog.Default())
	config, err := cfg.New()
	if err != nil {
		log.Fatal(err)
	}
	settings := meterbot.Settings{
		RawMessagesFilePath:  config.Get("RawMessagesFilePath"),
		RawMessagesBatchSize: config.GetInt("RawMessagesBatchSize"),
		SerialPortName:       config.Get("SerialPortName"),
		DSMRProtocolVersion:  config.GetInt("DSMRProtocolVersion"),
		Delivery:             config.GetBool("Delivery"),
		GasMeterDelay:        config.GetInt("GasMeterDelay"),
		Host:                 config.Get("Host"),
		Port:                 config.GetInt("Port"),
		DatabaseHost:         config.Get("DatabaseHost"),
		DatabaseName:         config.Get("DatabaseName"),
		DatabaseUsername:     config.Get("DatabaseUsername"),
		DatabasePassword:     config.Get("DatabasePassword"),
	}
	meterbot, err := meterbot.New(settings)
	if err != nil {
		log.Fatal(err)
	}
	log.Fatal(meterbot.Start())
}
