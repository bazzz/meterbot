package meterbot

// Settings holds the configuration for the Meterbot.
type Settings struct {
	// SerialPortName is the name of the serial port on the computer that is connected with the P1 port in the meter. On Unix this is usualy a device name like "/dev/cuaU0" whereas on Windows this may be "COM1" or similar.
	SerialPortName string

	// DSMRProtocolVersion is the version of the DSMR protocol that the meter uses. For version 2.2, set this to 2.
	DSMRProtocolVersion int

	// Delivery reports whether delivery readings should be stored in the database or not. If you do not deliver electricity to the net, set this to false to save database space.
	Delivery bool

	// RawMessagesFilePath is a path to the file where the raw DSMR messages should be saved. If empty, no raw messages will be saved.
	RawMessagesFilePath string

	// RawMessagesBatchSize defines the number of raw messages to store in memory before writing them to RawMessageFilePath in one go. This reduces the number of disk write operations. Default 250.
	RawMessagesBatchSize int

	// GasMeterDelay is the number of hours the gas meter reading should be delayed before registration. For example: Some Gas meters report usage between the hours 6 and 7 on the reading of 8:00. Unsure why. Setting this value to 1 will register this reading as 7:00 instead of 8:00.
	GasMeterDelay int

	// Host is the hostname to serve the web interface on.
	Host string

	// Port is the port number to server the web interface on.
	Port int

	// DatabaseHost is the host name of the database.
	DatabaseHost string

	// DatabaseName is the name of the database.
	DatabaseName string

	// DatabaseUsername is the user name to connect to the database.
	DatabaseUsername string

	// DatabasePassword is the password to connect to the database.
	DatabasePassword string
}
