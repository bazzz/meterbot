$(function() {
    $.getJSON("/gas/", function(result) {
        drawChart("gas", result);
    });
    $.getJSON("/electricity/", function(result) {
        drawChart("electricity", result);
    });
});

function drawChart(type, data) {
    let chart = new Chart(document.getElementById("chart-" + type).getContext("2d"), {
        type: "bar",
        data: {
            labels: data.labels,
            datasets: [{
                data: data.values,
                backgroundColor: data.colors,
                borderColor: data.borders,
                borderWidth: 1,
                borderRadius: 5
            }]
        },
        options: {
            plugins: {
                legend: {
                    display: false
                }
            },
            locale: "nl-nl",
            onClick: (e) => {
                const canvasPosition = Chart.helpers.getRelativePosition(e, chart);
                let day = data.labels.length - chart.scales.x.getValueForPixel(canvasPosition.x) -1;
                chart.destroy();
                $.getJSON("/" + type + "/?day=" + day, function(result) {
                    drawChart(type, result);
                });
            }  
        }
    });
}
