package meterbot

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

func (m *MeterBot) handleHome(w http.ResponseWriter, r *http.Request) {
	bag := newBag()
	bag.Set("title", appName)
	if err := templates.ExecuteTemplate(w, "home.gohtml", bag); err != nil {
		log.Print("ERROR: could not serve web:", err)
	}
}

func (m *MeterBot) handleGas(w http.ResponseWriter, r *http.Request) {
	var data []byte
	var err error
	if r.URL.Query().Get("day") != "" {
		day, err := strconv.Atoi(r.URL.Query().Get("day"))
		if err != nil {
			return
		}
		data, err = json.Marshal(m.GetDayGas(day))
	} else {
		data, err = json.Marshal(m.GetRecentDailyGas())
	}
	if err != nil {
		log.Println("ERROR: could not serialize:", err)
		return
	}
	w.Write(data)
}

func (m *MeterBot) handleElectricity(w http.ResponseWriter, r *http.Request) {
	var data []byte
	var err error
	if r.URL.Query().Get("day") != "" {
		day, err := strconv.Atoi(r.URL.Query().Get("day"))
		if err != nil {
			return
		}
		data, err = json.Marshal(m.GetDayElectricity(day))
	} else {
		data, err = json.Marshal(m.GetRecentDailyElectricity())
	}
	if err != nil {
		log.Println("ERROR: could not serialize:", err)
		return
	}
	w.Write(data)
}

type Bag struct {
	data map[string]interface{}
}

func newBag() *Bag {
	return &Bag{data: make(map[string]interface{})}
}

func (b *Bag) Get(name string) interface{} {
	return b.data[name]
}

func (b *Bag) Set(name string, value interface{}) {
	b.data[name] = value
}
